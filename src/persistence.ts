import { readFile } from 'fs/promises';
import { DB_PATH } from './constants';
import { Post } from './types';

export async function getAllPosts(): Promise<Post[]> {
  try {
    const file = await readFile(DB_PATH);
    const fileContent = file.toString();

    if (!fileContent) {
      return [];
    }

    const posts: Post[] = JSON.parse(fileContent);

    return posts;
  } catch (e) {
    throw e;
  }
}
